import React from 'react'

const Ancor = (props) => {
  return (
    <>
        <a className="text-blue-200 hover:text-white py-5 px-5">
            {props.name}
        </a>
    </>
  )
}

export default Ancor