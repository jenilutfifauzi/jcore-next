import React from "react";
import Head from "next/head";
import Navbar from "../Organisms/Navbar";

export default function Layout(props) {
  return (
    <div>
      <Head>
        <title>{ props.title }</title>
        <link rel="icon" href="https://avatars.githubusercontent.com/u/61114044?s=96&v=4" />
      </Head>
      <Navbar />
      <div className="container mt-5">
        { props.children }
      </div>
    </div>
  );
}
