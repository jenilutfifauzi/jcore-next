import Link from 'next/link'
import React from 'react'

export default function Navbar() {
  return (
    <div className='bg-blue-500'>
      <div className='container'>
        <div className="flex items-center justify-between">
          <div>
            <a className="text-white uppercase font-semibold tracking-tighter " href="" >
              { process.env.appName }
            </a>
          </div>
          <div className="flex items-center ">
            <Link href="/about" legacyBehavior>
                About
            </Link>
            <Link className="text-blue-200 hover:text-white py-5 px-5" href="/login" legacyBehavior>
                Login     
            </Link>
            <Link href="/register" legacyBehavior>
                Register
            </Link>
            <Link href="/products" legacyBehavior>
                Products
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}
