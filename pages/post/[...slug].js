import { useRouter } from "next/router"
import Layout from "../../components/Templates/Layout";


const Slug = () => {

    const router = useRouter();
    const {slug = []} = router.query;
    
  return (
    <Layout>
        <h1>Post Slug: {slug[0]} - {slug[1]}</h1>
    </Layout>
  )
}

export default Slug