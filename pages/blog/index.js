import React from 'react'
import { useRouter } from 'next/router'
import Layout from '../../components/Templates/Layout'

const Blog = () => {

    const router = useRouter();
    const {page, limit} = router.query;

  return (
    <Layout>
        <h1>Blog Page, page: {page} limit {limit}</h1>
    </Layout>
  )
}

export default Blog
