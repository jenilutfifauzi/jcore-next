import React from 'react'
import { useRouter} from 'next/router'
import Layout from '../../../components/Templates/Layout';

const BlogId = () => {

    const router = useRouter();
    const {blogId} = router.query;
    
  return (
    <Layout>
        <h1>Blog Dinamic Routes : {blogId}</h1>
    </Layout>
  )
}

export default BlogId