import React from 'react'
import { useRouter} from 'next/router'
import Layout from '../../../../components/Templates/Layout';

const CommentId = () => {

    const router = useRouter();
    const {commentId, blogId} = router.query;
    
  return (
    <Layout>
        <h1>Blog Dinamic Routes : {blogId} - {commentId}</h1>
    </Layout>
  )
}

export default CommentId;