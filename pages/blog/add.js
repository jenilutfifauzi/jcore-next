import { useRouter } from 'next/router';
import React from 'react'
import Layout from '../../components/Templates/Layout'

const Add = () => {
    const router = useRouter();
    const publish = () => {
        console.log('publisheed');
        router.push("/blog");
    }

  return (
    <Layout>
        <h2>Add new blog</h2>

        <button className="bg-red-600 rounded-lg text-white px-2 py-0" onClick={publish}>Publish</button>
    </Layout>
  )
}

export default Add