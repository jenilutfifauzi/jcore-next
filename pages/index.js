import React from 'react'
import Layout from '../components/Templates/Layout'

export default function Home() {
  return (
    <Layout title="Home">
      <h1 className="text-5xl font-bold text-blue-600">Hello World</h1>
    </Layout>
  )
}
