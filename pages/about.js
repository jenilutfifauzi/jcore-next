import React from 'react'
import Layout from '../components/Templates/Layout'


export default function Home() {
  return (
    <Layout title="About">
      <h1 className="text-5xl font-bold text-blue-600">About</h1>
    </Layout>
  )
}
