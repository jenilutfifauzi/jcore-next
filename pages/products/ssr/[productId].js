import Link from 'next/link'
import React from 'react'
import Layout from '../../../components/Templates/Layout'

const DetailProduct = ({product}) => {
  return (
    <Layout>
        <h3 className="bg-blue-500 rounded-lg px-2 py-1 mb-6 text-white">Detail Product</h3>
        <div className="bg-blue-400 rounded-lg px-2 py-1 text-white mb-8">
            <ul key={product.id}>
                <li>Nama Product : {product.title}</li>
                <li>Brand Product : {product.brand}</li>
                <li>Kategori Product : {product.category}</li>
                <li>Harga Product : {product.price}</li>
            </ul>
        </div>

        <div className="text-blue-600">
            <Link href='/products'>Kembali</Link>
        </div>
    </Layout>
  )
}

export default DetailProduct


export const getServerSideProps = async ({params}) => {
    const res = await fetch(process.env.baseUrl + params.productId);
    const data = await res.json();

    return {
        props: {
            product: data
        }
    }
}
