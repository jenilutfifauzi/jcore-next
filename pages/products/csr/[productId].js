import React from "react";
import Layout from "../../../components/Templates/Layout";
import { useEffect, useState } from "react";
import Loading from "../../../components/Atom/Loading";
import Link from "next/link";

const DetailProduct = () => {
  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch(process.env.baseUrl)
      .then((res) => res.json())
      .then((data) => {
        setData(data);
        setLoading(false);
      });
  }, []);

  if (isLoading) return <Layout><Loading /></Layout>;
  if (!data) return <p>No profile data</p>;

  return (
    <>
      <Layout>
        {data.products.map((item) => (
          <ul key={item.id}>
            <li>Nama Product : {item.title}</li>
            <li>Brand Product : {item.brand}</li>
            <li>Kategori Product : {item.category}</li>
            <li>Harga Product : {item.price}</li>
          </ul>
        ))}

        <div className="text-blue-600">
          <Link href="/products">Kembali</Link>
        </div>
      </Layout>
    </>
  );
};

export default DetailProduct;
