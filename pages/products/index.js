import React from "react";
import Button from "../../components/Atom/Button";
import Layout from "../../components/Templates/Layout";

const Products = ({ products }) => {
    
  return (
    <Layout>
      <h1 className="mb-7 text-lg font-semibold">List Products Page: </h1>
      <div className="grid grid-cols-6 gap-4 ">
        {products.products.map((item) => (
            <div className="" key={item.id}>
                <div className="p-6 rounded-lg shadow-lg bg-white max-w-sm ">
                    <h5 className="text-gray-900 text-xl leading-tight font-medium mb-2">
                        {item.title}
                    </h5>
                    <p className="text-gray-700 text-base mb-4 line-clamp-3 md:line-clamp-none">
                        {item.brand}
                    </p>
                    <p className="text-gray-700 text-base mb-4 line-clamp-3 md:line-clamp-none">
                        Rp. {item.price}
                    </p>

                    <div className="flex flex-col justify-between items-center gap-4">
                    <Button link={`/products/ssg/${item.id}`} name="test" />
                    <Button link={`/products/ssr/${item.id}`} name="test ssr" />
                    <Button link={`/products/csr/${item.id}`} name="test csr" />
                    </div>
                </div>
            </div>
        ))}
        </div>
    </Layout>
  );
};

export default Products;

export const getStaticProps = async () => {
  const res = await fetch(process.env.baseUrl);
  const data = await res.json();


  return {
    props: {
      products: data,
    },
  };
};
