import React from 'react'
import Layout from '../../../components/Templates/Layout'
import Link from 'next/link';

const DetailProduct = ({product}) => {
    console.log(process.env.baseUrl);
  return (
    <Layout>
        <h3 className="bg-blue-500 rounded-lg px-2 py-1 mb-6 text-white">Detail Product</h3>
        <div className="bg-blue-400 rounded-lg px-2 py-1 text-white mb-8">
            <ul key={product.id}>
                <li>Nama Product : {product.title}</li>
                <li>Brand Product : {product.brand}</li>
                <li>Kategori Product : {product.category}</li>
                <li>Harga Product : {product.price}</li>
            </ul>
        </div>

        <div className="text-blue-600">
            <Link href='/products'>Kembali</Link>
        </div>
    </Layout>
  )
}

export default DetailProduct

export const getStaticPaths = async () => {
    const res = await fetch(`https://dummyjson.com/products`);
    const data = await res.json();

    const paths = data.products.map((item) => ({
        params: {
            productId : `${item.id}`
        }
    }))

    return {
        paths : paths,
        fallback : false
    }
}


export const getStaticProps = async ({params}) => {
    const res = await fetch(process.env.baseUrl + params.productId);
    const data = await res.json();

    return {
        props: {
            product: data
        }
    }
}
