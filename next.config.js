/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,

  env : {
    appName : 'Learn Next.js',
    baseUrl : 'https://dummyjson.com/products/',
  },

  async rewrites(){
    return [
      {
        source: '/login',
        destination: '/auth/login',
      },
      {
        source: '/register',
        destination: '/auth/register',
      }
    ]
  }
}

module.exports = nextConfig
